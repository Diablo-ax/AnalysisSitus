project(asiEngine)

#------------------------------------------------------------------------------
# Common
#------------------------------------------------------------------------------

set (H_FILES
  asiEngine.h
  asiEngine_Base.h
  asiEngine_Clearance.h
  asiEngine_Curve.h
  asiEngine_Editing.h
  asiEngine_Features.h
  asiEngine_Isomorphism.h
  asiEngine_IV.h
  asiEngine_IVTopoItemSTEPWriterInput.h
  asiEngine_Model.h
  asiEngine_Octree.h
  asiEngine_Part.h
  asiEngine_PatchJointAdaptor.h
  asiEngine_RE.h
  asiEngine_STEPReaderOutput.h
  asiEngine_STEPWriterInput.h
  asiEngine_Tessellation.h
  asiEngine_Thickness.h
  asiEngine_TolerantShapes.h
  asiEngine_Triangulation.h
)
set (CPP_FILES
  asiEngine_Base.cpp
  asiEngine_Clearance.cpp
  asiEngine_Curve.cpp
  asiEngine_Editing.cpp
  asiEngine_Features.cpp
  asiEngine_Isomorphism.cpp
  asiEngine_IV.cpp
  asiEngine_IVTopoItemSTEPWriterInput.cpp
  asiEngine_Model.cpp
  asiEngine_Octree.cpp
  asiEngine_Part.cpp
  asiEngine_PatchJointAdaptor.cpp
  asiEngine_RE.cpp
  asiEngine_STEPReaderOutput.cpp
  asiEngine_STEPWriterInput.cpp
  asiEngine_Tessellation.cpp
  asiEngine_Thickness.cpp
  asiEngine_TolerantShapes.cpp
  asiEngine_Triangulation.cpp
)

#------------------------------------------------------------------------------
# Tree Functions
#------------------------------------------------------------------------------

set (func_H_FILES
  func/asiEngine_BuildEdgeFunc.h
  func/asiEngine_BuildOctreeFunc.h
  func/asiEngine_BuildPatchFunc.h
  func/asiEngine_CheckClearanceFunc.h
  func/asiEngine_CheckThicknessFunc.h
  func/asiEngine_CurvatureCombsFunc.h
  func/asiEngine_SmoothenCornersFunc.h
  func/asiEngine_SmoothenPatchesFunc.h
)
set (func_CPP_FILES
  func/asiEngine_BuildEdgeFunc.cpp
  func/asiEngine_BuildOctreeFunc.cpp
  func/asiEngine_BuildPatchFunc.cpp
  func/asiEngine_CheckClearanceFunc.cpp
  func/asiEngine_CheckThicknessFunc.cpp
  func/asiEngine_CurvatureCombsFunc.cpp
  func/asiEngine_SmoothenCornersFunc.cpp
  func/asiEngine_SmoothenPatchesFunc.cpp
)

#------------------------------------------------------------------------------
set (OCCT_LIB_FILES
  TKernel
  TKMath
  TKBRep
  TKOffset
  TKTopAlgo
  TKG2d
  TKG3d
  TKGeomBase
  TKGeomAlgo
  TKMesh
  TKShHealing
  TKFeat
  TKBool
  TKBO
  TKPrim
  TKBin
  TKBinL
  TKBinXCAF
  TKLCAF
  TKCDF
  TKCAF
  TKXCAF
  TKService
  TKXSBase
  TKSTEP
  TKIGES
  TKXDESTEP
  TKXDEIGES
)

#------------------------------------------------------------------------------

if (NOT BUILD_ALGO_ONLY)
  set (VTK_LIB_FILES
    vtkCommonCore-8.2
    vtkCommonDataModel-8.2
    vtkCommonExecutionModel-8.2
    vtkCommonMath-8.2
    vtkCommonTransforms-8.2
    vtkCommonMisc-8.2
    vtkFiltersCore-8.2
    vtkFiltersGeneral-8.2
    vtkFiltersSources-8.2
    vtkFiltersGeometry-8.2
    vtkFiltersParallel-8.2
    vtkFiltersExtraction-8.2
    vtkFiltersModeling-8.2
    vtkIOCore-8.2
    vtkIOImage-8.2
    vtkImagingCore-8.2
    vtkInteractionStyle-8.2
  )
endif()

#------------------------------------------------------------------------------
# Add sources
#------------------------------------------------------------------------------

foreach (FILE ${H_FILES})
  set (src_files ${src_files} ${FILE})
  source_group ("Header Files" FILES "${FILE}")
endforeach (FILE)

foreach (FILE ${CPP_FILES})
  set (src_files ${src_files} ${FILE})
  source_group ("Source Files" FILES "${FILE}")
endforeach (FILE)

foreach (FILE ${func_H_FILES})
  set (src_files ${src_files} ${FILE})
  source_group ("Header Files\\Tree Functions" FILES "${FILE}")
endforeach (FILE)

foreach (FILE ${func_CPP_FILES})
  set (src_files ${src_files} ${FILE})
  source_group ("Source Files\\Tree Functions" FILES "${FILE}")
endforeach (FILE)

#------------------------------------------------------------------------------
# Configure includes
#------------------------------------------------------------------------------

# Create include variable
set (asiEngine_include_dir_loc "${CMAKE_CURRENT_SOURCE_DIR};\
  ${CMAKE_CURRENT_SOURCE_DIR}/func;\
")
#
set (asiEngine_include_dir ${asiEngine_include_dir_loc} PARENT_SCOPE)

include_directories ( SYSTEM
                      ${asiEngine_include_dir_loc}
                      ${asiActiveData_include_dir}
                      ${asiAlgo_include_dir}
                      ${asiAsm_include_dir}
                      ${asiData_include_dir}
                      ${asiVisu_include_dir}
                      ${3RDPARTY_OCCT_INCLUDE_DIR}
                      ${3RDPARTY_EIGEN_DIR}
                      ${3RDPARTY_vtk_INCLUDE_DIR} )

if (USE_MOBIUS)
  include_directories(SYSTEM ${3RDPARTY_mobius_INCLUDE_DIR})
endif()

#------------------------------------------------------------------------------
# Create library
#------------------------------------------------------------------------------

add_library (asiEngine SHARED ${H_FILES} ${CPP_FILES} ${func_H_FILES} ${func_CPP_FILES})

#------------------------------------------------------------------------------
# Dependencies
#------------------------------------------------------------------------------

target_link_libraries(asiEngine asiAlgo asiAsm asiData)

if (NOT BUILD_ALGO_ONLY)
  message (STATUS "... Adding asiVisu dependency for asiEngine.")
  target_link_libraries(asiEngine asiVisu)
endif()

if (3RDPARTY_tbb_LIBRARY_DIR_DEBUG)
  link_directories(${3RDPARTY_tbb_LIBRARY_DIR_DEBUG})
else()
  link_directories(${3RDPARTY_tbb_LIBRARY_DIR})
endif()

foreach (LIB_FILE ${OCCT_LIB_FILES})
  if (WIN32)
    set (LIB_FILENAME "${LIB_FILE}${CMAKE_STATIC_LIBRARY_SUFFIX}")
  else()
    set (LIB_FILENAME "lib${LIB_FILE}${CMAKE_SHARED_LIBRARY_SUFFIX}")
  endif()

  if (3RDPARTY_OCCT_LIBRARY_DIR_DEBUG AND EXISTS "${3RDPARTY_OCCT_LIBRARY_DIR_DEBUG}/${LIB_FILENAME}")
    target_link_libraries (asiEngine debug ${3RDPARTY_OCCT_LIBRARY_DIR_DEBUG}/${LIB_FILENAME})
    target_link_libraries (asiEngine optimized ${3RDPARTY_OCCT_LIBRARY_DIR}/${LIB_FILENAME})
  else()
    target_link_libraries (asiEngine ${3RDPARTY_OCCT_LIBRARY_DIR}/${LIB_FILENAME})
  endif()
endforeach()

if (NOT BUILD_ALGO_ONLY)
  foreach (LIB_FILE ${VTK_LIB_FILES})
    if (WIN32)
      set (LIB_FILENAME "${LIB_FILE}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    else()
      set (LIB_FILENAME "lib${LIB_FILE}${CMAKE_SHARED_LIBRARY_SUFFIX}")
    endif()

    if (3RDPARTY_vtk_LIBRARY_DIR_DEBUG AND EXISTS "${3RDPARTY_vtk_LIBRARY_DIR_DEBUG}/${LIB_FILENAME}")
      target_link_libraries (asiEngine debug ${3RDPARTY_vtk_LIBRARY_DIR_DEBUG}/${LIB_FILENAME})
      target_link_libraries (asiEngine optimized ${3RDPARTY_vtk_LIBRARY_DIR}/${LIB_FILENAME})
    else()
      target_link_libraries (asiEngine ${3RDPARTY_vtk_LIBRARY_DIR}/${LIB_FILENAME})
    endif()
  endforeach()
endif()

#------------------------------------------------------------------------------
# Installation of Analysis Situs as a software
#------------------------------------------------------------------------------

if (NOT BUILD_ALGO_ONLY)
  install (TARGETS asiEngine CONFIGURATIONS Release        RUNTIME DESTINATION bin  LIBRARY DESTINATION bin  COMPONENT Runtime)
  install (TARGETS asiEngine CONFIGURATIONS RelWithDebInfo RUNTIME DESTINATION bini LIBRARY DESTINATION bini COMPONENT Runtime)
  install (TARGETS asiEngine CONFIGURATIONS Debug          RUNTIME DESTINATION bind LIBRARY DESTINATION bind COMPONENT Runtime)
endif()

#------------------------------------------------------------------------------
# Installation of Analysis Situs as a framework
#------------------------------------------------------------------------------

install (TARGETS asiEngine
         CONFIGURATIONS Release
         RUNTIME DESTINATION ${SDK_INSTALL_SUBDIR}bin COMPONENT Runtime
         ARCHIVE DESTINATION ${SDK_INSTALL_SUBDIR}lib COMPONENT Library
         LIBRARY DESTINATION ${SDK_INSTALL_SUBDIR}lib COMPONENT Library)

install (TARGETS asiEngine
         CONFIGURATIONS RelWithDebInfo
         RUNTIME DESTINATION ${SDK_INSTALL_SUBDIR}bini COMPONENT Runtime
         ARCHIVE DESTINATION ${SDK_INSTALL_SUBDIR}libi COMPONENT Library
         LIBRARY DESTINATION ${SDK_INSTALL_SUBDIR}libi COMPONENT Library)

install (TARGETS asiEngine
         CONFIGURATIONS Debug
         RUNTIME DESTINATION ${SDK_INSTALL_SUBDIR}bind COMPONENT Runtime
         ARCHIVE DESTINATION ${SDK_INSTALL_SUBDIR}libd COMPONENT Library
         LIBRARY DESTINATION ${SDK_INSTALL_SUBDIR}libd COMPONENT Library)

install (FILES ${H_FILES}      DESTINATION ${SDK_INSTALL_SUBDIR}include)
install (FILES ${func_H_FILES} DESTINATION ${SDK_INSTALL_SUBDIR}include)

if (MSVC)
  install (FILES ${PROJECT_BINARY_DIR}/../../${PLATFORM}${COMPILER_BITNESS}/${COMPILER}/bind/asiEngine.pdb DESTINATION ${SDK_INSTALL_SUBDIR}bind CONFIGURATIONS Debug)
endif()

install (FILES ${H_FILES}      DESTINATION ${SDK_INSTALL_SUBDIR}include)
install (FILES ${func_H_FILES} DESTINATION ${SDK_INSTALL_SUBDIR}include)
