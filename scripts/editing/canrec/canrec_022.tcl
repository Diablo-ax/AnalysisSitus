source $env(ASI_TEST_SCRIPTS)/editing/canrec/__begin

# Set working variables.
set datafile cad/milled/milled_005.stp

# Reference numbers.
set ref_nbSurfBezier     0
set ref_nbSurfSpl        20
set ref_nbSurfConical    49
set ref_nbSurfCyl        87
set ref_nbSurfOffset     0
set ref_nbSurfSph        28
set ref_nbSurfLinExtr    6
set ref_nbSurfOfRevol    0
set ref_nbSurfToroidal   2
set ref_nbSurfPlane      106
set ref_nbCurveBezier    0
set ref_nbCurveSpline    41
set ref_nbCurveCircle    235
set ref_nbCurveEllipse   4
set ref_nbCurveHyperbola 0
set ref_nbCurveLine      390
set ref_nbCurveOffset    0
set ref_nbCurveParabola  0

__convert-canonical
